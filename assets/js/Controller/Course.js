//Course
appH5.controller('CourseCtrl', function ($scope, $routeParams, $q, $http, APIForder, TmpObj, TmpValue , Alertify) {
    function PostJson(url, data) {
        var deferred = $q.defer();
        $http.post(url, data).success(function (d) {
            deferred.resolve(d);
        });
        return deferred.promise;
    }

    var data = {};

    TmpValue.post(APIForder + "?service=User.GetSession", data)
        .then(function ($rs) {
            $scope.seedata = $rs;
        });

    $scope.message = "Hello";
    $scope.InSidebar = "N";
    $scope.ContentForder = "templates/Course/";
    $scope.ContentHtml = $routeParams.Action + ".htm";
});

appH5.controller('CourseTeacherListCtrl', function ($scope, $routeParams, $q, $http, APIForder, TmpObj, TmpValue , Alertify) {
    function PostJson(url, data) {
        var deferred = $q.defer();
        $http.post(url, data).success(function (d) {
            deferred.resolve(d);
        });
        return deferred.promise;
    }
    var data = {};
    TmpValue.post(APIForder + "?service=User.GetSession", data)
        .then(function ($rs) {
            $scope.seedata = $rs;
            $scope.Search();
        });

    //func
    $scope.Search = function (data) {
        if (data == undefined)
            var data = {};
        data.this_day = $('#this_day').val();
        if (data.text == undefined)
            data.text = '';

        console.log(data);

        TmpValue.post(APIForder + "?service=Course.ViewList", data)
            .then(function ($rs) {
                console.log($rs);
                if ($rs.data[0].msg_state == 'Y') {
                    console.log($rs);
                    if ($rs.data["0"].this_day_arr != undefined && (data.text != '' || data.this_day != '')) {
                        var temp1 = $rs.data["0"].this_day_arr;
                        $rs.data["0"].tmp_teach_arr = {};
                        for (var key in temp1) {
                            //console.log(temp1[key]);
                            $rs.data["0"].tmp_teach_arr = temp1[key];
                        }
                    } else if ($rs.data["0"].this_day_arr == undefined && (data.text != '' || data.this_day != '')) {
                        $rs.data["0"].tmp_teach_arr = {};
                    }

                    $scope.teacher_data = $rs;

                } else {
                    Alertify.alert($rs.data[0].msg_text);
                }
            });
    };

    // top header menu 
    $scope.member = function () {
        $('body').css('overflow', 'hidden');
        $(".pop-up").show();
        $('.login-box').show();
    };
    // menber end 


    $scope.colsePopUp = function () {
        $('.booking , .pop-up , .member-box , .recommend-box , .nowUpVideo , .booking_dt').hide();
        $(".communityPopUp , .adminMember , .deleteUser , .deleteMenber , .removeAdmin , .quitCommnuity").hide();
        $('body').css('overflow', 'auto')
        $('.pop-up video').hide().attr('src', '')
        $('.mid').css('height', 'auto')
        $('.mid').css('overflow', 'auto')
    };
    // colsePopUp end

    $scope.langChange = function () {
        $('.lang-box').slideToggle(0);
    };
    // langChange end
    // langBoxHide
    $scope.langBoxHide = function () {
        $(document).scroll(function () {
            $('.lang-box').fadeOut()
        });
    };
    // langBoxHide end

    $scope.forgetPassword = function () {
        $('.member-box').hide();
        $('.forget-box').show();
    };
    // forgetPassword end

    $scope.noAccount = function () {
        $('.member-box').hide();
        $('.register-box').show();
    };
    // noAccount end

    $scope.backLogin = function () {
        $('.login-box').show();
        $('.register-box , .forget-box').hide();
    };
    // backLogin end
    
    // top header menu end

    $scope.ContentForder = "templates/Course/";
    $scope.ContentHtml = "ViewList.htm";

});

appH5.controller('CourseRateListCtrl', function ($scope, $routeParams, $q, $http, APIForder, TmpObj, TmpValue , Alertify) {
    function PostJson(url, data) {
        var deferred = $q.defer();
        $http.post(url, data).success(function (d) {
            deferred.resolve(d);
        });
        return deferred.promise;
    }
    //$scope.data = TmpValue;
    formdata = {};
    formdata.teach_id = $routeParams.teach_id;
    data = formdata;
    TmpValue.post(APIForder + "?service=User.GetSession", data)
        .then(function ($rs) {
            $scope.seedata = $rs;
            $scope.getRatelist();
            $scope.getTeachtext();
        });


    //func
    $scope.getRatelist = function () {

        TmpValue.post(APIForder + "?service=User.RateList", data)
            .then(function ($rs) {
                if ($rs.ret == 200) {
                    $scope.ratelist = $rs;
                } else {
                    return false;
                }
            });

    };
    $scope.getTeachtext = function () {
        TmpValue.post(APIForder + "?service=Course.TeacherText", data)
            .then(function ($rs) {
                console.log($rs);
                if ($rs.data[0].msg_state == 'Y') {
                    //console.log($rs);
                    $scope.teacher_data = $rs;
                    //TmpObj = $rs;
                } else {
                    Alertify.alert($rs.data[0].msg_text);
                }
            })
    };




    $scope.ContentForder = "templates/Course/";
    $scope.ContentHtml = "TeacherRateList.htm";

});

appH5.controller('CourseRateCtrl', function ($scope, $routeParams, $q, $http, APIForder, TmpObj, TmpValue) {
    function PostJson(url, data) {
        var deferred = $q.defer();
        $http.post(url, data).success(function (d) {
            deferred.resolve(d);
        });
        return deferred.promise;
    }
    formdata = {};
    formdata.code = $routeParams.code;
    data = formdata;


    TmpValue.post(APIForder + "?service=User.GetSession", data)
        .then(function ($rs) {
            $scope.seedata = $rs;
            $scope.getRateClass();
        });

    $scope.ContentForder = "templates/Course/";
    $scope.ContentHtml = "RateClass.htm";

    //rate

    $scope.rate = 3;
    $scope.max = 5;
    $scope.isReadonly = false;
    $scope.ratingStates = [
        {
            stateOn: 'glyphicon-ok-sign',
            stateOff: 'glyphicon-ok-circle'
        },
        {
            stateOn: 'glyphicon-star',
            stateOff: 'glyphicon-star-empty'
        },
        {
            stateOn: 'glyphicon-heart',
            stateOff: 'glyphicon-ban-circle'
        },
        {
            stateOn: 'glyphicon-heart'
        },
        {
            stateOff: 'glyphicon-off'
        }
    ];

    $scope.hoveringOver = function (value) {
        $scope.rate = value;
        $scope.percent = 100 * (value / $scope.max);
    };
    //end rate

    //func
    $scope.getRateClass = function () {
        TmpValue.post(APIForder + "?service=User.RateClass", data)
            .then(function ($rs) {
                console.log($rs)
                if ($rs.ret == 200) {
                    $scope.rate_class = $rs;
                } else {
                    return false;
                }
            });

    };
    $scope.RateLessonUp = function () {
        data.lesson_rate_note = $('.lesson_rate_note').val()
        data.teach_score = $scope.rate;
        TmpValue.post(APIForder + "?service=User.RateClassUP", data)
            .then(function ($rs) {
                console.log($rs)
                if ($rs.ret == 200) {
                    //alert('The score is complete');
                    location.href = 'Index';
                } else {
                    return false;
                }
            });

    };
    //end func


});

appH5.controller('CourseTeacherTextCtrl', function ($scope, $routeParams, $q, $http, APIForder, TmpObj, TmpValue,$sce,Alertify) {
    function PostJson(url, data) {
        var deferred = $q.defer();
        $http.post(url, data).success(function (d) {
            deferred.resolve(d);
        });
        return deferred.promise;
    }

    $scope.trustSrc = function (url) {
        return $sce.trustAsResourceUrl(url);
    }

    $scope.LoadingOpen = function () {
        $('.pop-up').show()
        $('.loading').show()
    };
    $scope.LoadingClose = function () {
        $('.pop-up').hide()
        $('.loading').hide()
    };

    //console.log($scope.data);
    //$scope.data = TmpValue;
    $scope.today = new Date();
    //$scope.teacher_data = TmpObj;

    formdata = {};
    formdata.teach_id = $routeParams.id;
    $scope.teach_id = $routeParams.id;
    data = formdata;


    //main 
    TmpValue.post(APIForder + "?service=User.GetSession", data)
        .then(function ($rs) {
            $scope.seedata = $rs;
            console.log($scope.seedata);
        });
    //TmpValue.post(APIForder + "?service=Course.TeacherText", data)
    TmpValue.post(APIForder + "?service=Course.ViewList", data)
        .then(function ($rs) {
            console.log('teacherviewlist');
            console.log($rs);
            if ($rs.data[0].msg_state == 'Y') {
                $scope.teacher_data = $rs;
            } else {
                Alertify.alert($rs.data[0].msg_text);
            }
        });
    TmpValue.post(APIForder + "?service=Course.TeacherText", data)
        .then(function ($rs) {
            console.log('teachertext');
            console.log($rs);
            if ($rs.data[0].msg_state == 'Y') {
                $scope.teacher_tmp_arr = $rs.data["0"].tmp_list;
                TmpObj.board_user_id = $scope.teacher_tmp_arr['0']['id'];
                TmpObj.friends_user_id = $scope.teacher_tmp_arr['0']['id'];

            } else {
                Alertify.alert($rs.data[0].msg_text);
            }
        }).then(function ($rs) {
            /*
            $scope.CheckFriendsSubscription($scope.teacher_tmp_arr['0']['id']);
            $scope.FuncFriendsList($scope.teacher_tmp_arr['0']['id']);
            $scope.FuncSubscriptionList($scope.teacher_tmp_arr['0']['id']);
            $scope.FuncBoardList($scope.teacher_tmp_arr['0']['id']);
            $scope.FuncBoardMedia($scope.teacher_tmp_arr['0']['id']);
            */
            
            angular.element(document).ready(function () {
                $scope.BookingPage();
            });
            
        });
    //end main 

    // 註冊 密碼
    $scope.pwdfun = function () {
        $(document).delegate('.pwdinput', 'change', function() {
            if($(this).val().length > 0 ){
                $(this).attr("type","password")
            }else{
                $(this).attr("type","")
            }
        });
    }

    //click func

    $scope.openMsgToggle = function (data) {
        $(".msg_warp:eq(" + this.$index + ")").toggleClass('pack-up');
    }

    $scope.sendPrivateMessage = function () {

        data = {};
        data.users_id = $scope.teach_id;
        TmpValue.post(APIForder + "?service=Message.createGroup", data)
            .then(function ($rs) {
                console.log($rs);
                if ($rs.ret == 200) {
                    if (typeof $rs.msg != 'undefined' && $rs.msg == '') {
                        window.location = WebForder + 'Message.newMsg/' + $scope.teach_id + '/' + $rs.data[0].datas.id
                    } else {
                        Alertify.alert("Please sign in");
                    }
                } else {
                    Alertify.alert("Please sign in");
                    //alert($rs.msg);
                }
            });
    };

    $scope.leftBoxShowMore = function () {
        $('.left_group').slideToggle(150)
    };

    //func
    $scope.BookingPage = function () {
        /*
        if($scope.teacher_tmp_arr[0].fan_ident_match != 'Y'){
            Alertify.alert('Different communities can not make an appointment');
            return;
        }
        */
        //bookingpage init
        //起始時間與結束時間
        $("#this_day").datepicker({
            dateFormat: 'yy-mm-dd',
            changeYear: true,
            changeMonth: true,
            minDate: 'today',
            maxDate: "+2m"
        });
        $(".this_day").datepicker({
            dateFormat: 'yy-mm-dd',
            changeYear: true,
            changeMonth: true,
            minDate: 'today',
            maxDate: "+2m"
        });
        //起始時間與結束時間


        $(".btn-b").click(function () {
            $('.pop-up').show();
            $('body').css('overflow', 'auto');
            $('.pop-up').css('position', 'absolute');
            $('.pop-up').css('overflow', 'auto');
            $('.booking').show();
            var _midH = $('.pop-up').innerHeight() - $('footer').innerHeight() - $('header').innerHeight() - 20;
            $('.mid').height(_midH);
            $('.mid').css('overflow', 'hidden');
            //$('.day-box input')=''
            $('.ng-scope li').removeClass('thisTime');
        });

        $(".booking .time-box ul li").click(function () {
            var $this = $(this);
            $(".booking .time-box ul li").removeClass('thisTime');
            $this.addClass('thisTime');
        });
        $('.class-books div').click(function () {
            var $this = $(this);
            $('.class-books div').removeClass('thisBook');
            $this.addClass('thisBook');

        });
        $(".recommend").click(function () {
            $('body').css('overflow', 'hidden');
            $('.member-box').hide();
            $(".pop-up").show()
            $(".recommend-box").show();
        });
        $('.booking .booking-btn').click(function () {
            if ($('.day-box input') != '' && $('.ng-scope li').hasClass('thisTime')) {
                //$('#CourseBookingCtrl').hide()
                $("html,body").animate({
                    scrollTop: 0
                }, 0);
                //$('.loading').show()
            }
        })

    };
    //end bookingpage init

    $scope.blackBgShow = function () {
        $('.blackBg').show();
        $('.po-msg-box .title .fa').show();
        $(".po-msg-box").css("z-index", "15")
    };
    // blackBgShow end

    $scope.popUpMenuShow = function () {
        var _eq = this.$index;
        $(".popupMenu").eq(_eq).addClass("showthis");
        $(".post_box").eq(_eq).addClass("zIndexUp");
        $('.blackBg').css('z-index', '13').show();
    };
    // popUpMenuShow end

    $scope.packUp = function () {
        $(".msg_box, .key_in_box").slideToggle(0);
    };
    // packUp end

    $scope.showThisImg = function () {
        $('.left .videos .video-box').click(function () {
            $('body').css('overflow', 'hidden');
            var $this = $(this),
                $src = $this.find('img').attr('src');

            $('.member-box').hide();
            $('.pop-up').show();
            $('.pop-up video').attr('src', $src);
            $('.pop-up img').show().attr('src', $src);
        })
    };
    // showThisImg end

    // top header menu 
    $scope.member = function () {
        $('body').css('overflow', 'hidden');
        $(".pop-up").show();
        $('.login-box').show();
    };
    // menber end 

    $scope.colsePopUp = function () {
        $('.booking , .pop-up , .member-box , .recommend-box , .nowUpVideo , .booking_dt').hide();
        $(".communityPopUp , .adminMember , .deleteUser , .deleteMenber , .removeAdmin , .quitCommnuity").hide();
        $('body').css('overflow', 'auto');
        $('.pop-up video , .pop-up .v_iBox img').attr('src', '').hide();
        $('.mid').css('height', 'auto');
        $('.mid').css('overflow', 'auto');
    };
    // colsePopUp end
    
    //colseThisMsg
    $scope.colseThisMsg = function () {
        $(".upVidepFinish").fadeOut()
    }
    //colseThisMsg END
    
    $scope.langChange = function () {
        $('.lang-box').slideToggle(0);
    };
    // langChange end
    // langBoxHide
    $scope.langBoxHide = function () {
        $(document).scroll(function () {
            $('.lang-box').fadeOut()
        });
    };
    // langBoxHide end

    $scope.forgetPassword = function () {
        $('.member-box').hide();
        $('.forget-box').show();
    };
    // forgetPassword end

    $scope.noAccount = function () {
        $('.member-box').hide();
        $('.register-box').show();
    };
    // noAccount end
    
    $scope.backLogin = function () {
        $('.login-box').show();
        $('.register-box , .forget-box').hide();
    };
    // backLogin end
    
    $scope.messageList = [];
    $scope.privateMessageList = [];
    $scope.privateMessageName = "";

    var data = {};
    TmpValue.post(APIForder + "?service=User.GetSession", data)
        .then(function ($rs) {
            $scope.seedata = $rs;
            console.log($scope.seedata);
            if ($rs.data[0].f_backend != null) {
                return TmpValue.post(APIForder + "?service=User.TeacherEdit", data);
            }
            else {
				return false;
                //alert('login Error 4');
                //location.href = 'User.Login';
            }

        }).then(function ($rs) {
            $scope.profile_data = $rs;
            return TmpValue.post(APIForder + "?service=Message.readMessageList", data);
        }).then(function ($rs) {

            $scope.messageList = $rs.data.message_list;
            console.log($scope.messageList);
        });



    $scope.showPrivateMsg = function (group_data) {
        console.log(group_data);
        $(".name_box").removeClass("nowSchoolMsg");
        //$(".private_msg_pup").removeClass("nowSchoolMsgShow");
        $(".msg_head_" + group_data.group_id).addClass("nowSchoolMsg");
        formdata = {};
        formdata.teach_id = group_data.group_id;
        data = formdata;
        TmpValue.post(APIForder + "?service=User.GetSession", data)
            .then(function ($rs) {
                $scope.seedata = $rs;
                if ($rs.data[0].f_backend != null) {
                    return TmpValue.post(APIForder + "?service=User.TeacherEdit", data);
                }
                else {
                    return;
                }

            }).then(function ($rs) {
                $scope.profile_data = $rs;
                return TmpValue.post(APIForder + "?service=Message.readMessageByGroupId&group_id=" + group_data.group_id, data);
            }).then(function ($rs) {
                $scope.privateMessageList = $rs.data.messageList;
                $scope.privateMessageName = group_data.group_name;
                console.log($rs.data);
            });
    };

    $scope.pushMessageList = function (data) {
        console.log(data);
        $scope.messageList.unshift(data);
    }
    // showPrivateMsg end

    $scope.showPrivateBox = function () {
        $(".private_msg_list , .privateBtn .fa").slideToggle(0)
        $(".privateBar").toggleClass("shadowB")
    };
    // showPrivateBox end

    // left position fixed
    $scope.leftBoxFixed = function () {
        var position = $('.leftFixed').offset();
        var y = position.top
        if ($(window).width() > 1024) {
            $(document).scroll(function () {
                var leftHeight = $(".leftFixed").innerHeight(),
                    newY = leftHeight - y + $("header").innerHeight()
                if ($(".leftFixed").height() > $(window).height()) {
                    if ($("body").scrollTop() > newY) {
                        $(".leftFixed").addClass("boxFixed")
                    } else {
                        if ($(".leftFixed").height() > $(window).height()) {
                            var _top = $(window).height() - $(".leftFixed").height() + $("header").innerHeight()
                            $(".leftFixed").css("top", _top)
                        }
                        if ($("body").scrollTop() <= newY) {
                            $(".leftFixed").removeClass("boxFixed")
                        }
                    }
                    if ($("body").scrollTop() > $(document).height() - ($(window).height() + $("footer").innerHeight() + 40)) {
                        var _top = $(window).height() - ($(".leftFixed").height() + $("footer").innerHeight() + 40)
                        $(".leftFixed").css("top", _top)
                    } else {
                        var _top = $(window).height() - ($(".leftFixed").height() + $("header").innerHeight())
                        $(".leftFixed").css("top", _top)
                    }
                } else {

                    if ($("body").scrollTop() <= $('.right').offset().top) {
                        $(".leftFixed").removeClass("boxFixed")
                    } else {
                        if ($(".leftFixed").height() < $(window).height()) {
                            if ($("body").scrollTop() > newY) {
                                $(".leftFixed").addClass("boxFixed")
                            } else {
                                if ($(".leftFixed").height() < $(window).height()) {
                                    var _top2 = $("header").innerHeight()
                                    $(".leftFixed").css("top", _top2)
                                }
                                if ($("body").scrollTop() <= newY) {
                                    $(".leftFixed").removeClass("boxFixed")
                                }
                            }
                        }
                    }
                }



            })
        }
    };
    // left position fixed END
    
    

    // live video
    $scope.liveVideo = function () {
        $(".livePopup").show();
        $("body").css("overflow", "hidden");

        // 判斷 瀏覽器
        var userAgent = navigator.userAgent;
        var isChrome = userAgent.indexOf("Chrome") > -1;
        //var video = document.createElement('video');

        if (isChrome) {
            //var videoObj = { "video": true };
            if (1) {
                //if (navigator.getUserMedia) {
                // Put video listeners into place
                // Standard
                //$(".camera").css("display", "flex")// show 提示視窗顯示
                //$(".camera .camera_ready").show()// show 偵測到鏡頭提示
                /*
                navigator.getUserMedia(videoObj, function () {
                    //關閉鏡頭
                    //$(".goLive").hide();
                    //$("#highest").hide();

                    navigator.getUserMedia(videoObj, function () {
                        //alert(1);
                        $(".camera").css("display", "flex")// show 提示視窗顯示
                        $(".camera .camera_ready").show()// show 偵測到鏡頭提示
                        //$(".goLive").show()// hide 解析度 直撥按鈕
                        //$("#highest").show();
                        //$(".bottomBox select , .bottomBox button").show()// hide 解析度 直撥按鈕
                    }, function () {
                    });

                }, function () {
                    $(".camera").css("display", "flex")// show 提示視窗顯示
                    $(".camera .no_camera").show()// show 沒有鏡頭提示
                    $(".bottomBox select , .bottomBox button").hide()// hide 解析度 直撥按鈕
                });
                */

            } else {
                $(".camera").css("display", "flex")// show 提示視窗顯示
                $(".camera .no_camera").show()// show 沒有鏡頭提示
                $(".bottomBox select , .bottomBox button").hide()// hide 解析度 直撥按鈕 
            }
        } else {
            $(".bottomBox select , .bottomBox button").hide()
            $(".notchrome").css("display", "flex")
        }


    }

    // browser
    
        $scope.$watch(function () { return TmpObj.page_num; }, function (newVal) {
            if (newVal != null && newVal != undefined ) {
                if(TmpObj.old_page_num  != null && TmpObj.old_page_num != undefined && TmpObj.old_page_num != newVal ){
                    TmpObj.old_page_num = newVal;
                    $scope.loadMsg();
                }
            }
        });

    $scope.loadMsg = function () {
        $(document).scroll(function (scrollBarA) {
            if ($(document).height() > $(window).height() * 2) {
                var scrollVal = $(this).scrollTop(),
                    docHeight = $(document).height()-800;

                if (scrollVal > docHeight) {
                    console.log(TmpObj.page_num);
                    $(".right button.more").click();
                    $(document).unbind(scrollBarA)
                }
            }
        })
        
        
    };
    
    // joinLive
    $scope.joinLive = function (live_id) {
            console.log(this.$index);     
            $('.joinlivebtn:eq('+this.$index+')').parents(".user-msg-box").addClass("joinlivePopup")
            $(".joinlivePopup , .closeJoinLive").show()
            if(live_id !=null){
                angular.element(document.getElementById('CommBoard')).scope().StartUserChatRoom('userchatroom', live_id);
            }
        $("body").css("overflow", "hidden")
    }
    
    $scope.joinLiveinit = function () {
        $('.joinlivebtn').unbind().click(function () {
            $(this).parents(".user-msg-box").addClass("joinlivePopup")
            $(".joinlivePopup , .closeJoinLive").show()
        })
    }
    
    // closeJoinLive
    $scope.closeJoinLive = function () {
        $('#userchatroom').html('');
        $(".user-msg-box").removeClass("joinlivePopup");
        $(".joinlivePopup , .closeJoinLive").hide();
        $("body").css("overflow", "auto")
    }


    $scope.goLive = function () {
        $(".livePopup .live .video ,  .livePopup .bottomBox select , .livePopup .live .video .liveTitle , .livePopup .bottomBox button.goLive , .livePopup .live .talkBox , .finishLive , .livePopup .live .video .nowislive , .closeLive").removeClass("set");
        angular.element(document.getElementById('CommBoard')).scope().CreateLive();
        //前端load live 
        $(".camera").hide();
        $(".liveIframeBox").show();


        $(window).bind("beforeunload", function (event) {
            angular.element(document.getElementById('CommBoard')).scope().StartLive(TmpObj.tmp.stream_key, 'N', TmpObj.tmp.live_id);
            return '';
        });


    }; //開始直播
    
    $scope.hideLiveIframe = function () {
        $(".liveIframeBox iframe , .hideIframe").hide();
        $(".showIframe").show();
    }
    $scope.showLiveIframe = function () {
        $(".liveIframeBox iframe , .hideIframe").show();
        $(".showIframe").hide();
    }

    $scope.closeLive = function () {
        $(".livePopup , .joinlivePopup").hide()
        $("body").css("overflow", "auto")
        $(".livePopup .live .video , .livePopup .bottomBox select , .livePopup .live .video .liveTitle , .livePopup .bottomBox button.goLive , .livePopup .live .talkBox , .finishLive , .livePopup .live .video .nowislive , .closeLive").addClass("set")
    };

    $scope.finishLive = function () {

          Alertify.confirm('End live video ?')
            .then(function () {
              angular.element(document.getElementById('CommBoard')).scope().StartLive(TmpObj.tmp.stream_key, 'N', TmpObj.tmp.live_id);                
              //Alertify.success('You are sure!');
            }, function () {
              return;
            });
            

        $(".finishText .spet1").css("display", "flex")
        //$(".finishText .spet2").css("display", "flex")
        //$(".finishLive").addClass("set")
    }; // 結束直撥流程 1.先顯示 spet1 loading 流程 loading 完後請 隱藏 spet1 顯示 spet2

    $scope.doneBtn = function () {
        $(".livePopup").hide()
        $(".finishText .spet1 , .finishText .spet2").hide()
        $("body").css("overflow", "auto")
        $(".livePopup .live .video , .livePopup .bottomBox select , .livePopup .live .video .liveTitle , .livePopup .bottomBox button.goLive , .livePopup .live .talkBox , .finishLive , .livePopup .live .video .nowislive , .closeLive").addClass("set")
    }; //  spet2 按鈕 儲存影片
    //end live
    
    //laout
    $scope.ContentForder = "templates/Course/";
    $scope.ContentHtml = "TeacherText.htm";

});

appH5.controller('CourseHomeCtrl', function ($scope, $routeParams, $q, $http, APIForder, TmpObj, TmpValue) {
    function PostJson(url, data) {
        var deferred = $q.defer();
        $http.post(url, data).success(function (d) {
            deferred.resolve(d);
        });
        return deferred.promise;
    }
    var data = {};
    TmpValue.post(APIForder + "?service=User.GetSession", data)
        .then(function ($rs) {
            $scope.seedata = $rs;
        });


    $scope.message = "Hello";
    $scope.InSidebar = "N";
    //ajax 
    $scope.ContentForder = "templates/Course/";
    $scope.ContentHtml = "ViewList.htm";
});

appH5.controller('CourseBookingCtrl', function ($scope, $routeParams, $q, $http, APIForder, TmpObj, TmpValue , Alertify ) {
    function PostJson(url, data) {
        var deferred = $q.defer();
        $http.post(url, data).success(function (d) {
            deferred.resolve(d);
        });
        return deferred.promise;
    }
    var data = {};
    TmpValue.post(APIForder + "?service=User.GetSession", data)
        .then(function ($rs) {
            $scope.seedata = $rs;
        });
    console.log($scope.teacher_data.data[0]);
    $scope.dateDatas = $scope.teacher_data.data[0];

    $scope.changeTime = function (event) {
        $("li").removeClass('thisTime');
        $(event.target.parentNode).addClass('thisTime');
    }

    $scope.changeBookingDate = function (teacher_id) {

        $("li").removeClass('thisTime');
        var data = new Object();
        data.this_day = $('.this_day').val();
        data.teach_id = $('.teach_id').val();
        $('.loading').show()
        TmpValue.post(APIForder + "?service=Course.ViewList", data)
            .then(function ($rs) {
                //todo need new class inthe top loading
                $('.loading').hide()
                console.log($rs);
                if ($rs.data[0].msg_state == 'Y') {
                    for (var key in value = $rs.data[0].this_day_arr) {
                        for (var key1 in value[key]) {

                            $scope.dateDatas = $rs.data[key1];
                            break;
                        }
                    }
                    //console.log($scope.dateDatas)

                    //TmpObj = $rs;
                } else {
                    Alertify.alert($rs.data[0].msg_text);
                }
            });
    }

    $scope.booking = function () {
        data.lesson_day = $('.this_day').val();
        if ($scope.data != undefined)
            data.cause = $scope.data.cause;

        data.teach_id = $('.teach_id').val();
        data.can_tutor_time = $('.thisTime span').attr('can_id');
        data.article_link = $('.thisBook img').attr('book_id');

        var error_msg = "";
        if (typeof $('.this_day').val() == 'undefined' || $('.this_day').val() == null || $('.this_day').val() == "") {
            error_msg += "appointment date\n";
        }

        if (typeof $('.teach_id').val() == 'undefined' || $('.teach_id').val() == null || $('.teach_id').val() == "") {
            error_msg += "School Number\n";
        }

        if (typeof $('.thisTime span').attr('can_id') == 'undefined' || $('.thisTime span').attr('can_id') == null || $('.thisTime span').attr('can_id') == "") {
            error_msg += "appointment time\n";
        }

        if (error_msg != "") {
            Alertify.alert("Please fill in：\n" + error_msg);
            return;
        }

        //console.log( data);
        //return;
        $('.booking_dt').hide()
        $('.loading').show()
		
        setTimeout(function () {
            $('.loading').hide();
//            $('.loadingTrue').show();
            Alertify.alert("booking is completed");
        }, 300);
        setTimeout(function () {
            //close ok box
            $('.pop-up').hide();
            $('.loading').hide();
//            $('.loadingTrue').hide();
        }, 800);
		
        TmpValue.post(APIForder + "?service=Course.StudentBooking", data)
            .then(function ($rs) {

                console.log($rs);
                //$scope.seedata = $rs;
                //todo 同一個學生同一個時間不能booking 考慮設法拿掉
                if ($rs.ret == 200) {

                    if ($rs.data[0].msg_state == 'Y') {
                        $scope.data.cause='';
                        $('.this_day').val('');
                        $('.thisTime span').attr('can_id','');
                        $('.thisBook img').attr('book_id','');

                        //alert($rs.data[0].msg_text);
                        //$(".close-btn , .yesBtn").click()
                    } else {
                        Alertify.alert($rs.data[0].msg_text);
                        //$(".close-btn , .yesBtn").click()
                    }
                } else {
                    Alertify.alert($rs.msg);
                    //$(".close-btn , .yesBtn").click()
                }


            });

        /*
        $('.this_day').val();
        $('.thisTime span').attr('can_id');
        $('.thisBook img').attr('book_id');
        */
        $('body').css('overflow', 'auto')
        $('.mid').css('height', 'auto')
    };

    $scope.bookingGoToConfirm = function () {
        //booking data
        data.lesson_day = $('.this_day').val();
        if ($scope.data != undefined)
            data.cause = $scope.data.cause;

        data.teach_id = $('.teach_id').val();
        data.can_tutor_time = $('.thisTime span').attr('can_id');
        data.article_link = $('.thisBook img').attr('book_id');
        //end booking data 
        //tmp data
        $scope.tmpcan_tutor_time_name = $('.thisTime span').text().trim();
        //end tmp data

        var error_msg = "";
        if (typeof $('.this_day').val() == 'undefined' || $('.this_day').val() == null || $('.this_day').val() == "") {
            error_msg += "appointment date\n";
        }

        if (typeof $('.teach_id').val() == 'undefined' || $('.teach_id').val() == null || $('.teach_id').val() == "") {
            error_msg += "teacher number\n";
        }

        if (typeof $('.thisTime span').attr('can_id') == 'undefined' || $('.thisTime span').attr('can_id') == null || $('.thisTime span').attr('can_id') == "") {
            error_msg += "appointment time\n";
        }
        if ($scope.data != undefined) {
            if ($scope.data.cause == "") {
                error_msg += "reason\n";
            }
        } else {
            error_msg += "reason\n";
        }


        if (error_msg != "") {
            Alertify.alert("Please fill in：\n" + error_msg);
            return;
        }
        $scope.data = data;
        $('.booking').hide()
        $('.booking_dt').show()
    }

    //
    $scope.ContentForder = "templates/Course/";
    $scope.ContentHtml = "ViewList.htm";
});

function onChangeBookingDate() {
    angular.element(document.getElementById('CourseBookingCtrl')).scope().changeBookingDate();
}

$(function () {
    $(".time-box .time-box ul li").click(function () {
        var $this = $(this)
        $(".time-box .time-box ul li").removeClass('thisTime')
        $this.addClass('thisTime')
    })
});