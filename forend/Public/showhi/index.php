<?php
/**
 * Demo 统一入口
 */
ini_set('always_populate_raw_post_data', '-1');
require_once dirname(__FILE__) . '/../init.php';

//装载你的接口
DI()->loader->addDirs(API_FORDER);
DI()->loader->addDirs('Library');
//`DI()->loader->loadFile('Library/SMSTW/APISDK.php');

//使用json api方式
//$tmp_arr = getallheaders();

//if(isset($tmp_arr['api_key'])){

    $HTTP_RAW_POST_DATA = file_get_contents('php://input');
    $HTTP_RAW_POST_DATA = $HTTP_RAW_POST_DATA ? $HTTP_RAW_POST_DATA : "{}";
    $tmp_json = json_decode($HTTP_RAW_POST_DATA, true);
    if(is_array($tmp_json)){
        DI()->request = new PhalApi_Request(array_merge($_REQUEST,$tmp_json)); 
        $_REQUEST = array_merge($_REQUEST,$tmp_json);
    }

//}
//exit;
//end 使用json api方式


/** ---------------- 响应接口请求 ---------------- **/

$api = new PhalApi();
$rs = $api->response();
$rs->output();

