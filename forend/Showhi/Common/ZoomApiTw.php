<?php

class Common_ZoomApiTw
{

    public function __construct()
    {
        $this->ZOOMAPI_KEY = 'b73b66a221ac6e796e1ab2c980a9e666KOTR';
        $this->ZOOMAPI_SECRET = 'e2114db609015b048c621af8396df804N73D';
        $this->ZOOMAPI_SERVICE_URL ='https://zoomnow.net/API/zntw_api.php';
        //host id
        $this->ZOOMAPI_USER_ID ='mQ_CDu1XTf-g2C_KFzO87g';
       
    }

    public function __destruct()
    {

    }


    public function meeting_create($topic,$host_id=null){

        if($host_id ==null){
            $host_id  = $this->ZOOMAPI_USER_ID;
        }
        //set
        $data["api"]="meeting_create";
        $data["host_id"]=$host_id;
        $data["topic"]="$topic";
        $data["type"]="3";
        $data["start_time"]="";
        $data["duration"]="";
        $data["timezone"]="";
        $data["password"]="";
        $data["option_jbh"]="true";
        $data["option_start_type"]="";
        $data["option_host_video"]="false";
        $data["option_participants_video"]="false";
        $data["option_audio"]="";
        ksort($data);
        $data["check_value"] = $this->MakeMacValue($data);
        $data["API_Key"] = $this->ZOOMAPI_KEY;
        $postFields = http_build_query($data);
        //end set
       return $tmp_meeting = $this->Zoom_Post($this->ZOOMAPI_SERVICE_URL,$postFields);
    }

    public function meeting_booking($topic,
    $start_time,$duration,$timezone,
    $option_start_type,$option_host_video='false',
    $option_participants_video = 'false',$option_jbh ='true',$host_id=null){

        if($host_id ==null){
            $host_id  = $this->ZOOMAPI_USER_ID;
        }

        //set
        $data["api"]="meeting_create";
        $data["host_id"]=$host_id;
        $data["topic"]="$topic";
        $data["type"]="2";
        $data["start_time"]="$start_time";
        $data["duration"]="$duration";
        $data["timezone"]="";
        $data["password"]="";
        $data["option_jbh"]="$option_jbh";
        $data["option_start_type"]="";
        $data["option_host_video"]="$option_host_video";
        $data["option_participants_video"]="$option_participants_video";
        $data["option_audio"]="";
        ksort($data);
        $data["check_value"] = $this->MakeMacValue($data);
        $data["API_Key"] = $this->ZOOMAPI_KEY;
        $postFields = http_build_query($data);
        //end set
       return $tmp_meeting = $this->Zoom_Post($this->ZOOMAPI_SERVICE_URL,$postFields);
    }

    public function meeting_list($host_id=null){
        if($host_id ==null){
            $host_id  = $this->ZOOMAPI_USER_ID;
        }
        //set
        $data["api"]="meeting_list";
        $data["host_id"]=$host_id;
        $data["page_size"]=30;
        ksort($data);
        $data["check_value"] = $this->MakeMacValue($data);
        $data["API_Key"] = $this->ZOOMAPI_KEY;
        $postFields = http_build_query($data);
        //end set


        return $tmp_data = $this->Zoom_Post($this->ZOOMAPI_SERVICE_URL,$postFields);
    }

    public function meeting_get($room_id,$host_id=null){
        if($host_id ==null){
            $host_id  = $this->ZOOMAPI_USER_ID;
        }

        //set
        $data["api"]="meeting_get";
        $data["host_id"]=$host_id;
        $data["id"]=$room_id;
        ksort($data);
        $data["check_value"] = $this->MakeMacValue($data);
        $data["API_Key"] = $this->ZOOMAPI_KEY;
        $postFields = http_build_query($data);
        //end set


        return $tmp_data = $this->Zoom_Post($this->ZOOMAPI_SERVICE_URL,$postFields);
    }

    public function meeting_just_delete($room_id,$host_id=null){
        if($host_id ==null){
            $host_id  = $this->ZOOMAPI_USER_ID;
        }

        //set
        $data["api"]="meeting_delete";
        $data["host_id"]=$host_id;
        $data["id"]=$room_id;
        ksort($data);
        $data["check_value"] = $this->MakeMacValue($data);
        $data["API_Key"] = $this->ZOOMAPI_KEY;
        $postFields = http_build_query($data);
        //end set
        return $tmp_data = $this->Zoom_Post($this->ZOOMAPI_SERVICE_URL,$postFields);
    }

    public function meeting_delete($room_id,$host_id=null){
        if($host_id ==null){
            $host_id  = $this->ZOOMAPI_USER_ID;
        }
        
        //set
        $data["api"]="meeting_end";
        $data["host_id"]=$host_id;
        $data["id"]=$room_id;
        ksort($data);
        $data["check_value"] = $this->MakeMacValue($data);
        $data["API_Key"] = $this->ZOOMAPI_KEY;
        $postFields = http_build_query($data);
        //end set
        $tmp_data = $this->Zoom_Post($this->ZOOMAPI_SERVICE_URL,$postFields);
        

        //set
        $data["api"]="meeting_delete";
        $data["host_id"]=$host_id;
        $data["id"]=$room_id;
        ksort($data);
        $data["check_value"] = $this->MakeMacValue($data);
        $data["API_Key"] = $this->ZOOMAPI_KEY;
        $postFields = http_build_query($data);
        //end set
        return $tmp_data = $this->Zoom_Post($this->ZOOMAPI_SERVICE_URL,$postFields);
    }

    public function user_list(){
        //set
        $data["api"]="user_list";

        ksort($data);
        $data["check_value"] = $this->MakeMacValue($data);
        $data["API_Key"] = $this->ZOOMAPI_KEY;
        $postFields = http_build_query($data);
        //end set
        return $tmp_data = $this->Zoom_Post($this->ZOOMAPI_SERVICE_URL,$postFields);

    }

    public function user_get($user_id){
        //set
        $data["api"]="user_list";
        $data["id"]=$user_id;

        ksort($data);
        $data["check_value"] = $this->MakeMacValue($data);
        $data["API_Key"] = $this->ZOOMAPI_KEY;
        $postFields = http_build_query($data);
        //end set


        return $tmp_data = $this->Zoom_Post($this->ZOOMAPI_SERVICE_URL,$postFields);
    }


    function Zoom_Post($url, $data) 
    {        
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_RETURNTRANSFER,1);
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_POST, 1);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        $response = curl_exec($curl);
        $err = curl_error($curl);
        $response_arr = (array) json_decode($response);
        return array('result' => $response_arr , 'code' => $err);
    }


    public function MakeMacValue($aryData)
    {
            $aryAPI=array();
            foreach($aryData as $k => $v)
            {
            if(trim($v)=="")
            continue;
            $aryAPI[$k]=trim($v);
            }
            $encode_str = "API_Key=" . $this->ZOOMAPI_KEY . "&" . urldecode(http_build_query($aryAPI)) . "&API_Secret=" .
            $this->ZOOMAPI_SECRET;
            $encode_str = strtolower($encode_str);
            $CheckMacValue = strtoupper(md5($encode_str));
            return $CheckMacValue;
    }
}






