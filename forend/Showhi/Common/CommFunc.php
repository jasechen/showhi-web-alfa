<?php

class Common_CommFunc
{

    public function __construct()
    {


    }

    public function __destruct()
    {

    }

//tools for db check
    public function laout_check($str_arr=null){
      $linki = $GLOBALS['linki'];
      if(is_array($str_arr)){
        foreach ($str_arr as $key=>$value){
          $str_arr[$key]=htmlspecialchars($value);
          $str_arr[$key]=stripslashes($value);
        }
      }
      elseif(!is_null($str_arr) ){ 
          $str_arr=htmlspecialchars($str_arr);
          $str_arr=stripslashes($str_arr);  
      }
      else{
        $str_arr=null;
      }
    return $str_arr;
    } 
    
    public function get_uuid() {
        return sprintf( '%04x%04x-%04x-%04x-%04x-%04x%04x%04x',
            // 32 bits for "time_low"
            mt_rand( 0, 0xffff ), mt_rand( 0, 0xffff ),

            // 16 bits for "time_mid"
            mt_rand( 0, 0xffff ),

            // 16 bits for "time_hi_and_version",
            // four most significant bits holds version number 4
            mt_rand( 0, 0x0fff ) | 0x4000,

            // 16 bits, 8 bits for "clk_seq_hi_res",
            // 8 bits for "clk_seq_low",
            // two most significant bits holds zero and one for variant DCE1.1
            mt_rand( 0, 0x3fff ) | 0x8000,

            // 48 bits for "node"
            mt_rand( 0, 0xffff ), mt_rand( 0, 0xffff ), mt_rand( 0, 0xffff )
        );
    }  
    
    public function getIP ()
    {
      if (getenv('HTTP_CLIENT_IP')) {
      $ip = getenv('HTTP_CLIENT_IP');
      } else if (getenv('HTTP_X_FORWARDED_FOR')) {
      $ip = getenv('HTTP_X_FORWARDED_FOR');
      } else if (getenv('REMOTE_ADDR')) {
      $ip = getenv('REMOTE_ADDR');
      } else {
      $ip = $_SERVER['REMOTE_ADDR'];
      }
      list($a, $b, $c, $d) = explode('.', $ip);
      $a=(int)$a;  $b=(int)$b;  $c=(int)$c;  $d=(int)$d;
      if($a<=255 && $a>=0 && $b<=255 && $b>=0 && $c<=255 && $c>=0 && $d<=255 && $d>=0)
      $ip="$a.$b.$c.$d";
      else
      $ip=false;
      return $ip;
    }
    
    
}






