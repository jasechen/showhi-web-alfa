function replaceAll(str, targetStr, replaceStr){
    var isContinue = true;
    while(isContinue){
        str = str.replace(targetStr,replaceStr);
        if(str.indexOf(targetStr) <= -1){
            isContinue = false;
        }
    }
    return str;
}

function chechUrlData(fildName , fildType , fildValue){
    if(typeof fildValue === 'undefined' || fildValue === null || fildValue === ":"+fildName){
        return false;
    }
    if(fildType === 'Number'){
        try {
            if(isNaN(fildValue)){
                return false;
            }
        }
        catch(err) {
            return false;
        }
    }
    return true;
    
}

module.exports.replaceAll = replaceAll;
module.exports.chechUrlData = chechUrlData;