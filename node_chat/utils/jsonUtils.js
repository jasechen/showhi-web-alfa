function packageResponse(success, message, data) {
    var result = {};
    result['success'] = success ? 'true' : 'false';
    result['msg'] = message;
    result['data'] = data == null ? "" : data;
    return JSON.stringify(result);
}


function getKeys(jsonObject){
    if (typeof jsonObject.keys !== "function") {
        return Object.keys(jsonObject);
    }
    return null;
}

module.exports.packageResponse = packageResponse;
module.exports.getKeys = getKeys;